# J.T. Doggzone Theme

A Tumblr theme tailored for my own personal use and design.

http://jtdoggzone.co.vu


## General Outline

### Pages
* Main/Index pages
* About
* Permalink post [type] page
  * Text
  * Photo[set]
  * Quote
  * Link
  * Chat
  * Audio
  * Video
  * Answer
* Ask / Submit


### Sections
* Sidebar
  * Icon
  * Title
  * Description
  * Status (active, partially inactive, inactive)
  * Navigation
    * Home
    * About / FAQ
    * Ask / Submit
    * Common tags
    * Archive
    * Theme Credit (for myself)
* Page content*

\* Described in next section.


### Page Content

#### Main / Index
* 10+ posts
* Jump pagination


#### Permalink
* Content
* Notes
* Related post
* Permalink pagination
