var toggleAskBox = function (className) {
  var askBox = $('.' + className);

  $('main').prepend(askBox);

  toggleBox(className);
};

var toggleBox = function (className) {
  var animatedBox = $('.' + className);

  if(animatedBox.hasClass('flag'))
    animatedBox.show(250);
  else
    animatedBox.slideUp(250);

  animatedBox.toggleClass('flag');
};

var loadBox = function (className) {
  toggleBox(className);
};
